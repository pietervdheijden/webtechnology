<html>
    <head>
        <meta charset="UTF-8">
        <title>Vacation guide</title>
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <table bgcolor="GREY" width="100%">
            <tr>
                <td></td>
                <?php $x = mt_rand(1,50);?>
                <td width="600px"><img src="<?php echo 'bannerpics/' . $x . '.jpg'; ?>" width="600px" height="210px"/></td>
                <td></td>
            </tr>
        </table>
        <table bgcolor="#F5F6CE" width="100%">
            <tr>
                <td></td>
                <td width = "800px">
                    <div style="text-align: center;">
                        <a href="http://localhost/index.php?city=" target="_blank">
                            <img src="http://i1005.photobucket.com/albums/af173/AndrewKingKong/title_zps48b7838f.png" border="0" alt="WebsiteTitel"/></a>
                    </div>
                    <form>
                        <input Type="BUTTON" VALUE="Home Page" ONCLICK="window.location.href = 'http://localhost/index.php?city='"> 
                    </form>
                    <form name="form" action="" method="get">
                        <input type="text" name="city"  id="subjectCity" placeholder=" Type your city "  value="">
                    </form>
                    <?php
                    require_once 'visited.php';
//Update city Index
//Also added some filters for names with spaces and different naming in dbpedia
//$city will be the entered word without double spaces/spaces in front or back of the word/words and without html tags
//$cityIndex wil be the same as $city except that spaces will be underscores
                    $city = ucspecial('-', ucwords(strtolower(str_replace('\\', '', preg_replace('/\s+/', ' ', trim(isset($_GET['city']) ? $_GET['city'] : ""))))));
                    if ($city == '\'s-gravenhage' || $city == 'Den Haag' || $city == '\'s Gravenhage')
                        $city = "The Hague";
                    else if ($city == 'Den Bosch')
                        $city = "'s Hertogenbosch";
                    $cityIndex = str_replace(' ', '_', str_replace('\'s ', '\'s-', $city));
                    if ($city == 'Best')
                        $cityIndex = "Best,_Netherlands";

                    if ($cityIndex == "") {
                        //displayTop(); //Displays the top places visited on the site
                    } else {
                        //counter($city);


                        echo "<div style='text-align: center;'><h1> Traveling to " . $city . "</h1><h2>Views:";
                        //echo (display($city)) ? display($city) : ''; //Displays amount of views a city has
                        echo "</h2></div><br><br>";
                        //BEGIN: Query opdracht die ik uitvoer 
                        define("RDFAPI_INCLUDE_DIR", "rdfapi-php/api/");
                        include RDFAPI_INCLUDE_DIR . "RDFAPI.php";
                        $client = ModelFactory::getSparqlClient("http://dbpedia.org/sparql");
                        $querystring = '
	SELECT ?latCity ?lonCity
	WHERE {
	<http://dbpedia.org/resource/' . $cityIndex . '> 
	<http://www.w3.org/2003/01/geo/wgs84_pos#lat> 
	?latCity .
	<http://dbpedia.org/resource/' . $cityIndex . '> 
	<http://www.w3.org/2003/01/geo/wgs84_pos#long> 
	?lonCity
	}LIMIT 1';
                        $query = new ClientQuery();
                        $query->query($querystring);
                        $result = $client->query($query);
                        //RESULT of QUERY
                        foreach ($result as $line) {
                            $latCity = $line['?latCity']; //lat van de city
                            $latValue = substr($latCity, 9, 9);
                            $lonCity = $line['?lonCity']; //lon van de city
                            $lonValue = substr($lonCity, 9, 9);
                        }
                        require 'nearbyCitiesCounter.php';
                        if ($cityCount == 0) {
                            
                        } else {
                            echo '<h3>�Nearby cities</h3>';
                            require "nearbyCities.php";
                            require "location.php";
			    echo "<br>";
                        }
                        require 'population.php';
                        if ($populationValue == 99) {
                            
                        } else {
                            echo '<h3>�Population</h3>';
                            echo $populationValue;
		            echo "<br><br>";
                        }
                        require 'website.php';
                        if ($websiteValue == "99") {
                            
                        } else {
                            echo "<h3>� Website </h3>";
                            echo "<a href='" . $websiteValue . "'>" . $websiteValue . "</a>";
			    echo "<br><br>";
                        }

                        echo '<h3>�Map of ' . $city . '</h3>';
                    }
                    ?>
                    <script>
                        var jArrayLat = <?php echo json_encode($latArray); ?>;
                        var jArrayLon = <?php echo json_encode($longArray); ?>;
                        var nameCity = '<?php echo $city; ?>';
                        var map;
                        function initialize() {
                            map = new google.maps.Map(document.getElementById('map_canvas'), {
                                zoom: 10,
                                center: new google.maps.LatLng(<?php echo floatval($latValue) ?>, <?php echo floatval($lonValue) ?>),
                                mapTypeId: google.maps.MapTypeId.ROADMAP});
                            var iconBase = 'http://i1005.photobucket.com/albums/af173/AndrewKingKong/WebtechApp/mapsicons/';
                            var icons = {
                                yourcity: {
                                    name: nameCity,
                                    icon: iconBase + 'RedpinAndrewSmall_zps8e8e077f.png'},
                                TUe: {
                                    name: 'TU/e',
                                    icon: iconBase + 'tueIcon_zps56025d91.png'},
                                smallcity: {
                                    name: 'Nearby City',
                                    icon: iconBase + 'GreenPinAndrewSmall_zps93b8c91c.png'}};

                            function addMarker(feature) {
                                var marker = new google.maps.Marker({
                                    position: feature.position,
                                    icon: icons[feature.type].icon,
                                    map: map});
                            }
                            var count = 0;
                            var features = [
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[0]), parseFloat(jArrayLon[0])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[1]), parseFloat(jArrayLon[1])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[2]), parseFloat(jArrayLon[2])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[3]), parseFloat(jArrayLon[3])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[4]), parseFloat(jArrayLon[4])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[5]), parseFloat(jArrayLon[5])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[6]), parseFloat(jArrayLon[6])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[7]), parseFloat(jArrayLon[7])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[8]), parseFloat(jArrayLon[8])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[9]), parseFloat(jArrayLon[9])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[10]), parseFloat(jArrayLon[10])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[11]), parseFloat(jArrayLon[11])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[12]), parseFloat(jArrayLon[12])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[13]), parseFloat(jArrayLon[13])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[14]), parseFloat(jArrayLon[14])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[15]), parseFloat(jArrayLon[15])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[16]), parseFloat(jArrayLon[16])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[17]), parseFloat(jArrayLon[17])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[18]), parseFloat(jArrayLon[18])), type: 'smallcity'},
                                {position: new google.maps.LatLng(parseFloat(jArrayLat[19]), parseFloat(jArrayLon[19])), type: 'smallcity'},
                                {position: new google.maps.LatLng(<?php echo floatval($latTue = 51.448055) ?>, <?php echo floatval($lonTue = 5.489722) ?>), type: 'TUe'},
                                {position: new google.maps.LatLng(<?php echo floatval($latValue) ?>, <?php echo floatval($lonValue) ?>), type: 'yourcity'}];
                            for (var i = 0, feature; feature = features[i]; i++) {
                                addMarker(feature);
                            }

                            var legend = document.getElementById('legend');
                            for (var key in icons) {
                                var type = icons[key];
                                var name = type.name;
                                var icon = type.icon;
                                var div = document.createElement('div');
                                div.innerHTML = '<img src="' + icon + '">       ' + name;
                                legend.appendChild(div);
                            }
                            map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
                        }
                        google.maps.event.addDomListener(window, 'load', initialize);
                    </script>
                    <?php
                    if ($cityIndex == "") {
                        
                    } else {
                        echo "<div id=" . 'map_canvas' . "></div><div id=" . 'legend' . ">";
                        echo "<h4><b>Legenda</b></h4></div>";
                        echo "<br>";

                        //grafiek 1
                        require 'TemperatureCheck.php';
                        if (count($maxTemperatures) == 0) {
                            
                        } else {
                            require 'Temperature.php';
                        }
                        echo "<br>";
                        
                        //grafiek 2
                        require 'RainDaysCheck.php';
                        if (count($neerslagDagen) == 0) {
                            
                        } else {
                            require 'RainDays.php';
                        }
                        echo "<br><br>";
                        
                        //grafiek 3
                        require 'RainMmCheck.php';
                        if (count($neerslagMm) == 0) {
                            
                        } else {
                            require 'RainMm.php';
                        }
                        echo "<br>";
                        
                        
                        require 'simple_html_dom.php';
                        echo '<h3>�Some images of ' . $cityIndex . '</h3>';
                        //@var $html type
                        $html = file_get_html('http://www.google.com/search?q=' . $cityIndex . '&safe=off&source=lnms&tbm=isch&sa=X&ei=KsrPUuK9JpOO7QbbyYGADQ&ved=0CAcQ_AUoAQ&biw=1911&bih=897');
                        $counting = 0;
                        while ($counting < 12) {
                            $image2 = $html->find('img', $counting);
                            echo '<img src="' . ($image2->src) . ' "  height="128" width="128">';
                            $counting = $counting + 1;
			    
                        }
			echo "<br><br>";

                        echo '<h3> Reactions </h3>';
                        $name = '';
                        $age = '';
                        $message = '';
                        $ename = '';
                        $eage = '';
                        $emessage = '';
                        if (1==1){     //$_POST[post] == 'Post') {
//                            $name = trim($_POST[name]);
//                            $age = $_POST[age];
//                            $message = $_POST[message];
//                            $regex[0] = '/^[a-z ]{0,}$/i';
//                            $regex[1] = '/^[0-9]{1,3}$/';
//                            $error = false;
//                            if (!empty($name) && !preg_match($regex[0], $name)) {
//                                $error = true;
//                                $ename = 'Only letters are allowed';
//                            }
//                            if (!empty($age) && !preg_match($regex[1], $age)) {
//                                $error = true;
//                                $eage = 'Only numbers are allowed';
//                            }
//                            if (empty($message)) {
//                                $error = true;
//                                $emessage = 'Message can not be empty';
//                            }
//                            if (!$error) {
//                                reaction($city, $name, $age, $message);
//                            } else {
                                ?>
<!--                    <table id='reaction'>
                        <form method='post'>
                            <tr><td>
                                    <input type='text' name='name' placeholder='Name' value='<?echo $name;?>'><div id='red'><?php echo $ename; ?></div>
                                    <input type='text' name='age' size='1' maxlength='3' placeholder='Age' value='<?echo $age;?>'><div id='red'><?echo $eage; ?></div>
                                </td></tr>
                            <tr><td>
                                    <textarea rows='8' cols='70' name='message' placeholder='Message' maxlength='4000' ><?php echo $message;?></textarea><div id='red'><?echo $emessage; ?></div>
                                    <input type='submit' name='post' value='Post'>
                                </td></tr>
                        </form>
                    </table>-->

                    <?php
                    }
//                    }
//                    else
//                    {
                    ?>
<!--                    <table id='reaction'>
                        <form method='post'>
                            <tr><td>
                                    <input type='text' name='name' placeholder='Name' value='<?echo $name;?>'><div id='red'><?echo $ename; ?></div>
                                    <input type='text' name='age' size='1' maxlength='3' placeholder='Age' value='<?echo $age;?>'><div id='red'><?echo $eage; ?></div>
                                </td></tr>
                            <tr><td>
                                    <textarea rows='8' cols='70' name='message' placeholder='Message' maxlength='4000' ><?php echo $message;?></textarea><div id='red'><?php echo $emessage; ?></div>
                                    <input type='submit' name='post' value='Post'>
                                </td></tr>
                        </form>
                    </table>-->
                    <?php
                    }
//                    echo "<br>";
//                    reactionDisplay($city);//Displays reactions
//                    }
//		    
//                    echo "<br><br><br>";
                    ?>

                </td>
                <td></td>
            </tr>
        </table>
	<br>
        <div style="text-align: center;">           
            <span style="border:1px solid darkslateblue;">Made by Andrew Noorden, Lisette Sanchez, Rick Pijnenburg, Pieter van der Heijden</span>
        </div>
    </body>
</html>
