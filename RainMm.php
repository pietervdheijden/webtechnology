        <script type='text/javascript' src='https://www.google.com/jsapi'></script>
        <script type='text/javascript'>
                        google.load('visualization', '1', {'packages': ['corechart']});
                        google.setOnLoadCallback(drawChart);


                        function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                                ['Month', 'Amount of rain fall (mm)'],
<?php
$monthsYear = array("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
$array_count = count($monthsYear);

for ($key_Number = 0; $key_Number < $array_count; $key_Number++) {

    $querystring = ' 
        SELECT ?rainMm 
        WHERE {  
              <http://dbpedia.org/resource/' . $cityIndex . '> <http://dbpedia.org/property/' . $monthsYear[$key_Number] . 'PrecipitationMm> ?rainMm }';

    $query = new ClientQuery();
    $query->query($querystring);
    $results = $client->query($query);

    foreach ($results as $lines) {
        $RainMm = $lines['?rainMm'];
        $month = ucfirst($monthsYear[$key_Number]);
        foreach ($RainMm as $subs) {
            if ($subs == "http://www.w3.org/2001/XMLSchema#double") {
                
            } else if ($subs == "http://www.w3.org/2001/XMLSchema#integer") {
                
            } else {
                if ($subs != "") {
                    $RainM = $subs;
                }
            }
        }
        
        echo "['" . $month . "', " . $RainM . " ],";
    }
}
?>

                            ]);

                            var options = {
                                title: 'Amount of rain fall per month',
                            };

                            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_rainMm'));

                            chart.draw(data, options);

                        }
                        ;


        </script>
        
        
        
        
                    <?php
            if ($cityIndex == "") {
                
            } else {
                
                echo "<div id=" . 'chart_div_rainMm' . "></div>";
            }
            ?>
