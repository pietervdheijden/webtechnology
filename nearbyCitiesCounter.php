<?php
    $nearbyCities = array();

    $client = ModelFactory::getSparqlClient("http://dbpedia.org/sparql");
    
    //north
    $querystring = '
        SELECT ?cityNO ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/north> ?cityNO     .
              ?cityNO <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

        $cityCount = 0;
    foreach ($result as $line) {
        $cityNO = $line['?cityNO'];
        $nameNO = $line['?name'];
        
        foreach($nameNO as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }

    //northwest
    $querystring = '
        SELECT ?cityNW ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/northwest> ?cityNW     .
              ?cityNW <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

    foreach ($result as $line) {
        $cityNW = $line['?cityNW']; // Steden ten noorden van de stad.
        $nameNW = $line['?name'];
        
        foreach($cityNW as $sub){
            if($sub != "") {array_push($nearbyCities, $sub);}
        }

        foreach($nameNW as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }

    //northeast
    $querystring = '
        SELECT ?cityNE ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/northeast> ?cityNE     .
              ?cityNE <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

    foreach ($result as $line) {
        $cityNE = $line['?cityNE']; // Steden ten noorden van de stad.
        $nameNE = $line['?name'];
        
        foreach($nameNE as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }

    //south
    $querystring = '
        SELECT ?citySO ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/south> ?citySO     .
              ?citySO <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

    foreach ($result as $line) {
        $citySO = $line['?citySO']; // Steden ten noorden van de stad.
        $nameSO = $line['?name'];
        
        foreach($nameSO as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }

    //soouthwest
    $querystring = '
        SELECT ?citySW ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/southwest> ?citySW     .
              ?citySW <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

    foreach ($result as $line) {
        $citySW = $line['?citySW']; // Steden ten noorden van de stad.
        $nameSW = $line['?name'];
    
        foreach($nameSW as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }

    //southeast
    $querystring = '
        SELECT ?citySE ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/southeast> ?citySE     .
              ?citySE <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

    foreach ($result as $line) {
        $citySE = $line['?citySE']; // Steden ten noorden van de stad.
        $nameSE = $line['?name'];
        
        foreach($nameSE as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }

    //west
    $querystring = '
        SELECT ?cityWE ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/west> ?cityWE     .
              ?cityWE <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

    foreach ($result as $line) {
        $cityWE = $line['?cityWE']; // Steden ten noorden van de stad.
        $nameWE = $line['?name'];
        
        foreach($nameWE as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }

    //east
    $querystring = '
        SELECT ?cityEA ?name
        WHERE { 
              {<http://dbpedia.org/resource/' . $cityIndex. '> 
              <http://dbpedia.org/property/east> ?cityEA     .
              ?cityEA <http://xmlns.com/foaf/0.1/name> ?name }
              }';

    $query = new ClientQuery();
    $query->query($querystring);
    $result = $client->query($query);

    foreach ($result as $line) {
        $cityEA = $line['?cityEA']; // Steden ten noorden van de stad.
        $nameEA = $line['?name'];
        
        foreach($nameEA as $sub){
            if ($sub == "en"){}
            else {
                if($sub != "") {$cityCount++;}
            }
        }
    }
?>