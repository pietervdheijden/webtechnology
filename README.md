Webtechnology
=============
>This was my project for the course Webtechnology (2ID60) of the TUE.

>It is a simple vacation guide which search for information like cities nearby, location, pictures, etc.
It uses sparql which queries dbpedia (wikipedia database) and a HTML-wrapper of Google pictures for the images.
