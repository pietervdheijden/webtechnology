        <script type='text/javascript' src='https://www.google.com/jsapi'></script>
        <script type='text/javascript'>
                        google.load('visualization', '1', {'packages': ['corechart']});
                        google.setOnLoadCallback(drawChart);


                        function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                                ['Month', 'Max Temp.', 'Min Temp.'],
<?php
$monthsYear = array("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
$array_count = count($monthsYear);
$temperatureMax = array();

for ($key_Number = 0; $key_Number < $array_count; $key_Number++) {

    $querystring = ' 
        SELECT ?maxtemp ?mintemp 
        WHERE {  
              <http://dbpedia.org/resource/' . $cityIndex . '> <http://dbpedia.org/property/' . $monthsYear[$key_Number] . 'HighC> ?maxtemp . 
              <http://dbpedia.org/resource/' . $cityIndex . '> <http://dbpedia.org/property/' . $monthsYear[$key_Number] . 'LowC> ?mintemp }';

    $query = new ClientQuery();
    $query->query($querystring);
    $results = $client->query($query);

    foreach ($results as $lines) {
        $maxTemp = $lines['?maxtemp'];
        $minTemp = $lines ['?mintemp'];
        $month = ucfirst($monthsYear[$key_Number]);
        foreach ($maxTemp as $subs) {
            if ($subs == "http://www.w3.org/2001/XMLSchema#double") {
                
            } else if ($subs == "http://www.w3.org/2001/XMLSchema#integer") {
                
            } else {
                if ($subs != "") {
                    $maxT = $subs;
                }
            }
        }
        foreach ($minTemp as $subs) {
            if ($subs == "http://www.w3.org/2001/XMLSchema#double") {
                
            } else if ($subs == "http://www.w3.org/2001/XMLSchema#integer") {
                
            } else {
                if ($subs != "") {
                    $minT = $subs;
                }
            }
        }
        
        echo "['" . $month . "', " . $maxT . ", ". $minT . " ],";
    }
}
?>
            

                            ]);

                            var options = {
                                title: 'Maximum and minimum temperature per month',
                            };

                            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_temp'));

                            chart.draw(data, options);

                        }
                        ;


        </script>
        
        
                 <?php
            if ($cityIndex == "") {
                
            } else {
                echo '<h3>Temperatures</h3>';
                echo "<div id=" . 'chart_div_temp' . "></div>";
            }
            ?>
       