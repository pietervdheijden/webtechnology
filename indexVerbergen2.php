<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Vacation guide</title>
        <style type="text/css">
            #map_canvas {width: 600px; 
                         height: 400px;}
            #legend {background: white;padding: 10px}

        </style>
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <link rel="stylesheet" href="style.css">

    </head>
    <body bgcolor="GREY">
        <table width="100%">
            <tr>
                <td>&nbsp;</td>
                <?php 
                $x = mt_rand(1,50);
                ?>
                <td width="600px"><img src="<?php echo 'bannerpics/' . $x . '.jpg'; ?>" width="600px" height="150px"/></td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td>&nbsp;</td>
                <td width = "800px">

                    <table width="100%">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <div style="text-align: center;">
                                <h1>Vacation Guide</h1>
                                </div>
                                <form name="form" action="" method="get">
                                    <input type="text" name="city"  id="subjectCity" placeholder=" Type your city "  value="">
                                </form>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>

        <?php
        //Update city Index
        $cityIndex = ucwords(strtolower($UID = isset($_GET['city']) ? $_GET['city'] : ""));


        define("RDFAPI_INCLUDE_DIR", "/rdfapi-php/api/");
        include(RDFAPI_INCLUDE_DIR . "RDFAPI.php");

        if ($cityIndex == "") {
            
        } else {
            ?>
            <div style="text-align: center;">
                <h1> Traveling to <?php echo $cityIndex ?></h1>

            </div>
            <?php
            /*
             *  BEGIN: Query opdracht die ik uitvoer 
             */
            $client = ModelFactory::getSparqlClient("http://dbpedia.org/sparql");

            $querystring = '

                             SELECT ?latCity ?lonCity
                                WHERE {
                                    	<http://dbpedia.org/resource/' . $cityIndex . '> 
                                	<http://www.w3.org/2003/01/geo/wgs84_pos#lat> 
                                                                        	?latCity .
                                    	<http://dbpedia.org/resource/' . $cityIndex . '> 
                                  	<http://www.w3.org/2003/01/geo/wgs84_pos#long> 
                                         ?lonCity
                                        }LIMIT 1';

            $query = new ClientQuery();
            $query->query($querystring);
            $result = $client->query($query);

            /*
             * 
             * RESULT of QUERY
             */
            foreach ($result as $line) {
                $latCity = $line['?latCity']; //lat van de city
                $latValue = substr($latCity, 9, 9);
                $lonCity = $line['?lonCity']; //lon van de city
                $lonValue = substr($lonCity, 9, 9);
            }
            require 'nearbyCitiesCounter.php';
            if ($cityCount == 0){}
            else {
                echo '<h3> Nearby cities</h3>';
                require "nearbyCities.php";
                require "location.php";
            }
            
        }
        if ($cityIndex == "") {
            
        } else {

            
            require 'population.php';
            if ($populationValue == 99){}
            else {
                echo '<h3> Population</h3>';
                echo $populationValue;
            }

            

            require 'website.php';
            if ($websiteValue == "99"){}
            else {
                echo "<h3>  Website </h3>";
                echo "<a href='" . $websiteValue . "'>" . $websiteValue . "</a>";
            }

            echo '<h3> Map of ' . $cityIndex . '</h3>';
        }
        ?>
        <script>


            var jArrayLat = <?php echo json_encode($latArray); ?>;
            var jArrayLon = <?php echo json_encode($longArray); ?>;
            var nameCity = '<?php echo $cityIndex; ?>';

        </script>

        <script>
            var map;
            function initialize() {
                map = new google.maps.Map(document.getElementById('map_canvas'), {
                    zoom: 10,
                    center: new google.maps.LatLng(<?php echo floatval($latValue) ?>, <?php echo floatval($lonValue) ?>),
                    mapTypeId: google.maps.MapTypeId.ROADMAP});

                var iconBase = 'http://i1005.photobucket.com/albums/af173/AndrewKingKong/WebtechApp/mapsicons/';
                var icons = {
                    yourcity: {
                        name: nameCity,
                        icon: iconBase + 'RedpinAndrewSmall_zps8e8e077f.png'},
                    TUe: {
                        name: 'TU/e',
                        icon: iconBase + 'tueIcon_zps56025d91.png'},
                    smallcity: {
                        name: 'Nearby City',
                        icon: iconBase + 'GreenPinAndrewSmall_zps93b8c91c.png'}};

                function addMarker(feature) {
                    var marker = new google.maps.Marker({
                        position: feature.position,
                        icon: icons[feature.type].icon,
                        map: map})
                }






                var count = 0;


                var features = [
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[0]), parseFloat(jArrayLon[0])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[1]), parseFloat(jArrayLon[1])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[2]), parseFloat(jArrayLon[2])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[3]), parseFloat(jArrayLon[3])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[4]), parseFloat(jArrayLon[4])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[5]), parseFloat(jArrayLon[5])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[6]), parseFloat(jArrayLon[6])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[7]), parseFloat(jArrayLon[7])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[8]), parseFloat(jArrayLon[8])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[9]), parseFloat(jArrayLon[9])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[10]), parseFloat(jArrayLon[10])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[11]), parseFloat(jArrayLon[11])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[12]), parseFloat(jArrayLon[12])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[13]), parseFloat(jArrayLon[13])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[14]), parseFloat(jArrayLon[14])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[15]), parseFloat(jArrayLon[15])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[16]), parseFloat(jArrayLon[16])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[17]), parseFloat(jArrayLon[17])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[18]), parseFloat(jArrayLon[18])), type: 'smallcity'},
                    {position: new google.maps.LatLng(parseFloat(jArrayLat[19]), parseFloat(jArrayLon[19])), type: 'smallcity'},
                    {position: new google.maps.LatLng(<?php echo floatval($latTue = 51.448055) ?>, <?php echo floatval($lonTue = 5.489722) ?>), type: 'TUe'},
                    {position: new google.maps.LatLng(<?php echo floatval($latValue) ?>, <?php echo floatval($lonValue) ?>), type: 'yourcity'}];
                for (var i = 0, feature; feature = features[i]; i++) {
                    addMarker(feature);
                }

                var legend = document.getElementById('legend');
                for (var key in icons) {
                    var type = icons[key];
                    var name = type.name;
                    var icon = type.icon;
                    var div = document.createElement('div');
                    div.innerHTML = '<img src="' + icon + '">       ' + name;
                    legend.appendChild(div);
                }

                map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>

        <table width="100"><tr><td>&nbsp;</td><td>


<?php
if ($cityIndex == "") {
    
} else {

    echo "<div id=" . 'map_canvas' . "></div><div id=" . 'legend' . ">";
    echo "<b>Legenda</b></div>";
    require 'Grafiek.php';
    require 'simple_html_dom.php';


    echo '<h3> Some images of ' . $cityIndex . '</h3>';
    $html = file_get_html('https://www.google.com/search?q=' . $cityIndex . '&safe=off&source=lnms&tbm=isch&sa=X&ei=KsrPUuK9JpOO7QbbyYGADQ&ved=0CAcQ_AUoAQ&biw=1911&bih=897');
    $countimg = 0;

    while ($countimg < 12) {
        $image2 = $html->find('img', $countimg);
        echo '<img src="' . ($image2->src) . ' "  height="128" width="128">';
        $countimg = $countimg + 1;
    }
    
    require 'Temperature.php';
   if (count($maxTemperatures) == 0) {
       
       
  
    }
else {
    echo '<h3> Temperatures </h3>';
    echo "<div id=" . 'chart_div' . "></div>";
}
}
?>

                </td><td>&nbsp;</td></tr>
    </table>




    <br/><br/><br/><br/><br/><br/><br/>
    <br/><br/><br/><br/><br/><br/><br/>
</td>
<td>&nbsp;</td>
</tr>
</table>
<div style="text-align: center;">
    <span style="border:1px solid darkslateblue;">
        Made by Andrew Noorden, Lisette Sanchez, Rick Pijnenburg, Pieter van der Heijden
    </span>
</div>


</body>
</html>
