        <script type='text/javascript' src='https://www.google.com/jsapi'></script>
        <script type='text/javascript'>
                        google.load('visualization', '1', {'packages': ['corechart']});
                        google.setOnLoadCallback(drawChart);


                        function drawChart() {
                            var data = google.visualization.arrayToDataTable([
                                ['Month', 'Rainy Days'],
<?php
$monthsYear = array("jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec");
$array_count = count($monthsYear);

for ($key_Number = 0; $key_Number < $array_count; $key_Number++) {

    $querystring = ' 
        SELECT ?rainDays 
        WHERE {  
              <http://dbpedia.org/resource/' . $cityIndex . '> <http://dbpedia.org/property/' . $monthsYear[$key_Number] . 'PrecipitationDays> ?rainDays }';

    $query = new ClientQuery();
    $query->query($querystring);
    $results = $client->query($query);

    foreach ($results as $lines) {
        $RainDays = $lines['?rainDays'];
        $month = ucfirst($monthsYear[$key_Number]);
        foreach ($RainDays as $subs) {
            if ($subs == "http://www.w3.org/2001/XMLSchema#double") {
                
            } else if ($subs == "http://www.w3.org/2001/XMLSchema#integer") {
                
            } else {
                if ($subs != "") {
                    $RainD = $subs;
                }
            }
        }
        
        echo "['" . $month . "', " . $RainD . " ],";
    }
}
?>

                            ]);

                            var options = {
                                title: 'Amount of rainy days per month',
                            };

                            var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_rainDays'));

                            chart.draw(data, options);

                        }
                        ;


        </script>
        
        
        
        
                    <?php
            if ($cityIndex == "") {
                
            } else {
                echo '<h3>Precipitation</h3>';
                echo "<div id=" . 'chart_div_rainDays' . "></div>";
            }
            ?>
