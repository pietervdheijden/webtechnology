<!DOCTYPE html>
<html>
    <head>
        <style>
            #map_canvas {
                width: 500px;
                height: 400px;
            }
            #legend {
                background: white;
                padding: 10px;
            }
        </style>
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
        
            <script>
      var map;
      function initialize() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
          zoom: 16,
          center: new google.maps.LatLng(51.91722, 5.23064),
          mapTypeId: google.maps.MapTypeId.ROADMAP});

        var iconBase = 'http://i1005.photobucket.com/albums/af173/AndrewKingKong/WebtechApp/mapsicons/';
        var icons = {
          yourcity: {
            name: 'Your City',
            icon: iconBase + 'RedpinAndrewSmall_zps8e8e077f.png'},
          bigcity: {
            name: 'Big City',
            icon: iconBase + 'GreenPinAndrewMiddle_zps18811f2f.png'},
          smallcity: {
            name: 'Small City',
            icon: iconBase + 'GreenPinAndrewSmall_zps93b8c91c.png'}};

        function addMarker(feature) {
          var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map});}

        var features = [
            {position: new google.maps.LatLng(-30.91721, 151.22630),                    type: 'smallcity'}, 
            {position: new google.maps.LatLng(-33.91539, 151.22820),                    type: 'smallcity'},
            {position: new google.maps.LatLng(-33.91662347903106, 151.22879464019775),  type: 'bigcity'}, 
            {position: new google.maps.LatLng(-33.916365282092855, 151.22937399734496), type: 'bigcity'}, 
            {position: new google.maps.LatLng(51.91727341958453, 5.23348314155578),  type: 'yourcity' }];

        for (var i = 0, feature; feature = features[i]; i++) {addMarker(feature);}

        var legend = document.getElementById('legend');
        for (var key in icons) {
          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + icon + '"> ' + name;
          legend.appendChild(div);}

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);
      }

      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    </head>
    <body>

        <table width="250%">

           

                 <div id="map_canvas"></div>
                 <div id="legend"> Andrews Legenda</div>

            </tr>
        </table>

    </body>
</html>